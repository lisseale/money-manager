import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import streamlit as st


# load the Excel file with data
class DataLoader:
    def __init__(self):
        """Initialization of DataLoader."""
        self.df = pd.DataFrame()

    def load_data(self, file_path, sheet):
        """Loads the data from provided Excel file into Dataframe."""
        try:
            xls = pd.ExcelFile(file_path)
            self.df = pd.read_excel(xls, sheet)
            return self.df
        except FileNotFoundError:
            st.error(f"Error: The file '{file_path}' was not found.")
            self.df = pd.DataFrame()  # empty
        except ValueError:
            st.error(f"Error: The sheet named '{sheet}' could not be found in the file '{file_path}'.")
            self.df = pd.DataFrame()  # empty
        except Exception as e:
            st.error(f"Error: Other error occurred: {e}")
            self.df = pd.DataFrame()  # empty


# perform data cleaning
class DataProcessor:
    def __init__(self, dataframe):
        """Initialization of DataProcessor with a DataFrame."""
        self.df = dataframe

    def clean_data(self):
        """CLeans the DataFrame by dropping rows with missing values, also removing the not needed ID column."""
        self.df.dropna(inplace=True)
        # drop the first column, not used
        if 'ID' in self.df.columns:
            self.df.drop(labels=['ID'], axis=1, inplace=True)


# create a plot
class DataVisualizer:
    def __init__(self, dataframe):
        """Initialization of DataVisualizer with a DataFrame."""
        self.df = dataframe

    def plot_histogram(self, column):
        """Creates plot for the given column."""
        plt.figure(figsize=(10, 6))
        if pd.api.types.is_numeric_dtype(self.df[column]):
            sns.histplot(self.df[column], color='blue', edgecolor='green')
            plt.title(f'Histogram of {column}')
            plt.xlabel(column)
            plt.ylabel('Frequency')
        else:
            sns.countplot(x=self.df[column], palette='viridis')
            plt.title(f'Count Plot of {column}')
            plt.xlabel(column)
            plt.ylabel('Count')
        st.pyplot(plt)

    # calculate values to show under the plot
    def calculate_statistics(self, column, statistic):
        """Calculates selected statistic value for a column that is numeric."""
        if not pd.api.types.is_numeric_dtype(self.df[column]):
            return f"Could not compute {statistic} for non-numeric data."
        elif statistic == 'Median':
            value = self.df[column].median()
        elif statistic == 'Mean':
            value = self.df[column].mean()
        elif statistic == 'Mode':
            value = self.df[column].mode()[0]
        elif statistic == 'Variance':
            value = self.df[column].var()
        else:
            value = None
        return value

    def interpret_column(self, column):
        """Gets an interpretation for the chosen column."""
        if column == 'Income':
            return "Higher income can be associated with higher likelihood of purchasing a bike because of higher disposable income."
        elif column == 'Education':
            return "Higher education levels can be associated with greater income and more education about the impact of cycling on health and the environment, which could lead to more bike purchases."
        elif column == 'Age' or column == 'Age Brackets':
            return "Middle-aged people could be more likely to purchase a bike based on a stable income and need for an easy individual sport activity."
        elif column == 'Commute Distance':
            return "Shorter commute distances could be more easily covered by bike as a mode of transportation because of convenience, cost, and environmental savings."
        elif column == 'Cars':
            return "People with cars are still purchasing bikes, which suggests that bikes are used for purposes beyond commuting, for example, fitness."
        elif column == 'Region':
            return "Purchasing a bike might be influenced by regional cultural attitudes and the availability of bike-safe roads."
        elif column == 'Martial Status':
            return "Martial status could influence bike purchases, in a way that married people could incline to family-oriented activities or shared transportation options."
        elif column == 'Gender':
            return "Gender differences might reveal different preferences, possibly influenced by lifestyle or social factors."
        elif column == 'Children':
            return "Having children might affect purchasing a bike, as families would opt for biking as an family activity or mode of transportation."
        elif column == 'Occupation':
            return "Occupation could mean big difference in income level and lifestyle choices, impacting the possibility of purchasing a bike."
        elif column == 'Home Owner':
            return "Homeowners could have more financial stability and disposable income, possibly leading to higher bike purchases"
        elif column == 'Purchased Bike':
            return "This column indicates if an individual has purchased a bike, which is the factor analyzed in relation to others."
        else:
            return "This column doesn't have an obvious interpretation. Based on the data, you can try to explore it."


# used to create and set up the final page
class PageCreator():
    def __init__(self):
        """Initialization of BikeSalesApp."""
        self.loader = DataLoader()
        self.processor = None
        self.visualizer = None
        self.df = pd.DataFrame()

    def load_and_clean_data(self):
        """Load and clean data."""
        st.title('Bike sales visualisation')

        # load the data
        path = 'Bike Sales Dashboard.xlsx'
        sheet = 'bike_buyers'
        self.df = self.loader.load_data(path, sheet)

        # if its empty, stop right away
        if self.df.empty:
            st.stop()

        # clean data
        self.processor = DataProcessor(self.df)
        self.processor.clean_data()
        self.df = self.processor.df

    def apply_filters(self):
        """Apply filters to the dataframe."""
        # show the filter option when the page starts
        st.sidebar.header('Filters')

        # filters by category
        if 'Region' in self.df.columns:
            selected_region = st.sidebar.multiselect(
                'Select Region(s)',
                options=self.df['Region'].unique(),
                default=list(self.df['Region'].unique())
            )
            if selected_region:
                self.df = self.df[self.df['Region'].isin(selected_region)]

        if 'Gender' in self.df.columns:
            selected_gender = st.sidebar.multiselect(
                'Select Gender(s)',
                options=self.df['Gender'].unique(),
                default=list(self.df['Gender'].unique())
            )
            if selected_gender:
                self.df = self.df[self.df['Gender'].isin(selected_gender)]

        if 'Commute Distance' in self.df.columns:
            selected_commute_distances = st.sidebar.multiselect(
                'Select Commute Distance(s)',
                options=self.df['Commute Distance'].unique(),
                default=list(self.df['Commute Distance'].unique())
            )
            if selected_commute_distances:
                self.df = self.df[self.df['Commute Distance'].isin(selected_commute_distances)]

        # numeric filters
        st.sidebar.header('Numeric Filters')

        if 'Age' in self.df.columns:
            age_min, age_max = float(self.df['Age'].min()), float(self.df['Age'].max())
            selected_age_range = st.sidebar.slider(
                'Select Age Range',
                min_value=int(age_min),
                max_value=int(age_max),
                value=(int(age_min), int(age_max))
            )
            self.df = self.df[self.df['Age'].between(selected_age_range[0], selected_age_range[1])]

        if 'Income' in self.df.columns:
            income_min, income_max = float(self.df['Income'].min()), float(self.df['Income'].max())
            selected_income_range = st.sidebar.slider(
                'Select Income Range',
                min_value=int(income_min),
                max_value=int(income_max),
                value=(int(income_min), int(income_max))
            )
            self.df = self.df[self.df['Income'].between(selected_income_range[0], selected_income_range[1])]

    def display_data(self):
        """Display the dataframe and visualizations."""
        st.dataframe(self.df)

        # select visualization
        self.visualizer = DataVisualizer(self.df)
        column = st.selectbox('Select column for histogram', self.df.columns)
        statistic = st.selectbox('Select value to display', ['Median', 'Mean', 'Mode', 'Variance'])
        if st.button('Plot'):
            self.visualizer.plot_histogram(column)
            # based on chosen column show corresponding plot
            if pd.api.types.is_numeric_dtype(self.df[column]):
                value = self.visualizer.calculate_statistics(column, statistic)
                st.write(f'The {statistic} value for {column} is {value}')
            else:
                st.write(f"Statistics cannot be computed for non-numeric column {column}.")
            # short text for each chosen column
            interpretation = self.visualizer.interpret_column(column)
            st.write(f"**Interpretation:** {interpretation}")

    def run(self):
        """Run the app."""
        self.load_and_clean_data()
        self.apply_filters()
        self.display_data()


if __name__ == '__main__':

    try:
        page = PageCreator()
        page.run()
    except Exception as e:
        print(f'There was en error running the script: {e}')
