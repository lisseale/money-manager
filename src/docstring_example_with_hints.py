from typing import List

def calculate_average(numbers: List[float]) -> float:
    """
    Calculate the average of a list of numbers.

    Parameters:
    numbers (List[float]): A list of numbers to calculate the average from.

    Returns:
    float: The average of the numbers in the input list.
    
    Example:
    >>> calculate_average([1, 2, 3, 4, 5])
    3.0
    """
    if not numbers:
        raise ValueError('The input list must not be empty')
    
    total = sum(numbers)
    average = total / len(numbers)
    return average
